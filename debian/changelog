ghextris (0.9.0-5) unstable; urgency=medium

  * Team upload.
  * Source-only upload to allow testing migration. 
  * debian/control: Bump Standards-Version to 4.6.0.
  * debian/control: Bump debhelper compat to v13.
  * debian/compat: Dropped, not needed.

 -- Boyuan Yang <byang@debian.org>  Sun, 07 Nov 2021 17:09:29 -0500

ghextris (0.9.0-4) unstable; urgency=medium

  * Reintroduce package, keep games team as maintainer, set myself
    as uploader. (Closes: 989894)
  * Port from obsolete python2/gtk2/gnome2 to python3/python3-gi/gtk3/glade
    (Closes: 790576)
  * Update standards version to 4.5.1
    + Remove Debian menu stuff (the package has a desktop file).
  * Change vcs fields in debian/control to point to salsa
  * Bump debhelper compat and debhelper dependency to 12.
  * Remove "A" from start of short description

 -- Peter Michael Green <plugwash@debian.org>  Tue, 15 Jun 2021 18:51:54 +0000

ghextris (0.9.0-3) unstable; urgency=low

  * Rename Build-Depends-Indep to Build-Depends (Policy 7.6)
  * Change my E-Mail Address
  * Add Vcs: Headers

 -- Christoph Egger <christoph@debian.org>  Fri, 02 Apr 2010 18:04:05 +0200

ghextris (0.9.0-2) unstable; urgency=low

  * Adopt package for the Debian Games Team (Closes: #543736, #553486)
  * Add patch from Peter de Wachter to fix crash on any-i386 (Closes: #555491)
  * Keeping all NMU Patches (Closes: #379318)
  * Make package non-native based on -1 debian tarball
  * Port to dh 7 from manual Style
  * Drop encoding key from Desktop File
  * Convert to Source 3.0 (quilt), nicely avoids repacking for the debian/
    in »upstream« tarball

 -- Christoph Egger <debian@christoph-egger.org>  Wed, 11 Nov 2009 13:39:56 +0100

ghextris (0.9.0-1.3) unstable; urgency=high

  [ Jason Harrison ]
  * Non-maintainer upload.
  * Renamed #!/usr/bin/python2.3 to #!/usr/bin/python. Closes: #403874
  * Changed program to use random instead of whrandom which is depreciated.

 -- Andreas Barth <aba@not.so.argh.org>  Tue, 26 Dec 2006 11:13:20 +0000

ghextris (0.9.0-1.2) unstable; urgency=low

  * NMU.
  * Remove explicit dependency on python2.3.

 -- Matthias Klose <doko@debian.org>  Fri, 27 Oct 2006 05:25:53 +0200

ghextris (0.9.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename dependencies for python transition (Closes: 377080).
  * Fix some deprecation warnings.
  * Add French translation (Closes: #334483).
  * Add Swedish translation (Closes: #343301, #345175).

 -- Luk Claes <luk@debian.org>  Sat, 22 Jul 2006 20:01:33 +0200

ghextris (0.9.0-1) unstable; urgency=low

  * New upstream version, new Debian packaging.

 -- Mikko Rauhala <mjr@iki.fi>  Thu, 13 May 2004 04:37:00 +0300
