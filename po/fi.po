# Ghextris 0.9
# Copyright (C) 2004 Mikko Rauhala
# This file is distributed under the same license as the Ghextris package.
# Mikko Rauhala <mjr@iki.fi>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.9\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2004-05-13 01:48+0300\n"
"PO-Revision-Date: 2004-05-13 01:48+0300\n"
"Last-Translator: Mikko Rauhala <mjr@iki.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ghextris.glade.h:1
msgid "Ghextris"
msgstr "Ghextris"

#: ghextris.glade.h:2
msgid "© 2004 Mikko Rauhala <mjr@iki.fi>"
msgstr "© 2004 Mikko Rauhala <mjr@iki.fi>"

#: ghextris.glade.h:3
msgid "translator_credits"
msgstr "Mikko Rauhala <mjr@iki.fi>"

#: ghextris.py:334
msgid "Score"
msgstr "Pisteet"

#: ghextris.py:335
msgid "High score"
msgstr "Paras tulos"
