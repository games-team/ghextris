#!/bin/sh

if [ "f$1" == f ]; then
    echo "usage: $0 prefix [real_prefix]"
    echo
    echo "where prefix is the directory under which to install (eg. /usr, /usr/local)"
    echo "and real_prefix is the directory under which the program is eventually"
    echo "to run (the same as prefix by default)."
    echo "You probably don't want to touch the latter by hand."
    echo 
    exit
fi

PREFIX="$1"
BINDIR="$PREFIX/games"
MANDIR="$PREFIX/share/man/man6"
SHAREDIR="$PREFIX/share/games/ghextris"
LOCALEDIR="$PREFIX/share/locale"
APPDIR="$PREFIX/share/applications"

if [ "f$2" == f ]; then
    REALPREFIX="$PREFIX"
else
    REALPREFIX="$2"
fi

REALSHAREDIR="$REALPREFIX/share/games/ghextris"

umask 022

echo "Installing program files"

mkdir -m 755 -p "$BINDIR" || exit 1
mkdir -m 755 -p "$SHAREDIR" || exit 1
mkdir -m 755 -p "$APPDIR" || exit 1
mkdir -m 755 -p "$MANDIR" || exit 1

sed "s#^SHAREDIR.*#SHAREDIR = \"$REALSHAREDIR\"#" < ghextris.py > "$SHAREDIR/ghextris.py" || exit 1
chmod 755 "$SHAREDIR/ghextris.py"
sed "s#^Icon=.*#Icon=$REALSHAREDIR/ghextris.png#" < ghextris.desktop > "$APPDIR/ghextris.desktop" || exit 1
chmod 644 "$APPDIR/ghextris.desktop"

cp ghextris.glade "$SHAREDIR" || exit 1
cp ghextris.png "$SHAREDIR" || exit 1
cp ghextris-icon.xpm "$SHAREDIR" || exit 1
cp ghextris.man "$MANDIR/ghextris.6" || exit 1

# We just link the .py into the games directory for now; in the future,
# this step may be replaced by optional installation of a setgid games
# wrapper program for scorekeeping
ln -sf ../share/games/ghextris/ghextris.py "$BINDIR"/ghextris || exit 1

echo "Checking for msgfmt"

if which msgfmt > /dev/null; then
    echo "msgfmt found, installing translations"

    for a in po/*.po; do
	LANG="`basename $a .po`"
	echo "- Language $a"
	mkdir -m 755 -p "$LOCALEDIR/$LANG/LC_MESSAGES"
	msgfmt -o "$LOCALEDIR/$LANG/LC_MESSAGES/ghextris.mo" "$a" || exit 1
    done
else
    echo "msgfmt not found; translations not installed"
fi

echo "Done."
