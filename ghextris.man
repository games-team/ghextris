.TH ghextris 6 "May 20, 2004" "X Version 11"
.SH NAME
ghextris \- Hextris game for GNOME
.SH SYNOPSIS
.B ghextris
.SH DESCRIPTION
.IR ghextris
is a Python/GNOME reimplementation of the semi-free Xhextris by
David Markley. The object of the game is basically the same as in Tetris;
use the pieces that fall down from the top of the game area to construct
fully filled lines, which will then disappear to make room for more pieces.
The twist, as first introduced by Markley, is that the pieces are composed
of hexagonal blocks, so your visualization skills will get an extra workout.

.SH RULES
The pieces can be moved left and right with the respective arrow keys
and rotated with the up and down arrow keys. If you're satisfied with
where a piece is going, it can be dropped by pressing the space bar.
Fully formed lines will disappear from the playing field. 
The game will end when the pile of pieces reaches the top of the game area.

.SS Score
You get 1 point for every line that you drop a piece, and
100, 200, 400 or 800 points for 1, 2, 3 or 4 simultaneously destroyed
lines, respectively.

.SH AUTHOR
Mikko Rauhala <mjr@iki.fi>

.SH COPYRIGHT
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
.PP 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
